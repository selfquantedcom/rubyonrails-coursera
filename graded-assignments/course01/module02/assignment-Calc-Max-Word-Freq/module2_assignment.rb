#Implement all parts of this assignment within (this) module2_assignment2.rb file

#Implement a class called LineAnalyzer.
class LineAnalyzer
  #Implement the following read-only attributes in the LineAnalyzer class. 

  #* highest_wf_count - a number with maximum number of occurrences for a single word (calculated)
  #* highest_wf_words - an array of words with the maximum number of occurrences (calculated)
  #* content          - the string analyzed (provided)
  #* line_number      - the line number analyzed (provided)

  #Add the following methods in the LineAnalyzer class.
  #* initialize() - taking a line of text (content) and a line number
  #* calculate_word_frequency() - calculates result

  attr_accessor :highest_wf_count 
  attr_accessor :highest_wf_words   
  attr_accessor :content
  attr_accessor :line_number


  def initialize(content, line_number) 
   #Implement the initialize() method to:
  #* take in a line of text and line number
  #* initialize the content and line_number attributes
  #* call the calculate_word_frequency() method.   
  @content = content
  @line_number = line_number
  @highest_wf_count 
  calculate_word_frequency()
end

def calculate_word_frequency()
    #Implement the calculate_word_frequency() method to:
    #* calculate the maximum number of times a single word appears within
    #  provided content and store that in the highest_wf_count attribute.
    #* identify the words that were used the maximum number of times and
    #  store that in the highest_wf_words attribute.

    #load @content as array. Take care of an inidivdual word being passed     
    begin
      text = @content.split(" ")   
    rescue
      text = [@content]
    end

    #convert to lower case      
    text.each{|value| value.downcase!}
    
    #establish word,frequency pairs
    hash = Hash.new(0)
    text.each{|key| hash[key] += 1}
    
    #get highest occurence and the corresponding value    
    @highest_wf_words = []
    @highest_wf_words.push(hash.key(hash.values.max))    
    @highest_wf_count = hash.max_by{|k,v| v}[1]    

    #check for other elements with the highest frequency to return multiple items where appropriate
    counter = 0
    while(true)
      if(hash.length()==1)
       break
     end

     hash.delete(@highest_wf_words[counter])      
     other_val = hash.max_by{|k,v| v}[1]       

     if (other_val == @highest_wf_count)
       next_element = hash.key(hash.values.max)                
       @highest_wf_words.push(next_element)
       counter += 1         
     else
       break
     end      
   end               
 end

end

#  Implement a class called Solution. 
class Solution
    # Implement the following read-only attributes in the Solution class.
  #* analyzers - an array of LineAnalyzer objects for each line in the file
  #* highest_count_across_lines - a number with the maximum value for highest_wf_words attribute in the analyzers array.
  #* highest_count_words_across_lines - a filtered array of LineAnalyzer objects with the highest_wf_words attribute 
  #  equal to the highest_count_across_lines determined previously.

    # Implement the following methods in the Solution class.
  #* analyze_file() - processes 'test.txt' intro an array of LineAnalyzers and stores them in analyzers.
  #* calculate_line_with_highest_frequency() - determines the highest_count_across_lines and 
  #  highest_count_words_across_lines attribute values
  #* print_highest_word_frequency_across_lines() - prints the values of LineAnalyzer objects in 
  #  highest_count_words_across_lines in the specified format
  attr_reader :analyzers
  attr_reader :highest_count_across_lines
  attr_reader :highest_count_words_across_lines  

  def initialize()    
    @analyzers = []
    @highest_count_across_lines
    @highest_count_words_across_lines 
  end

  def analyze_file()

  # Implement the analyze_file() method() to:
  #* Read the 'test.txt' file in lines 
  #* Create an array of LineAnalyzers for each line in the file

    #read test.txt by line. Store line, line_num into an array of analyzers
    line_num=0
    text=File.open('test.txt').read
    text.gsub!(/\r\n?/, "\n")
    text.each_line do |line|
      obj = LineAnalyzer.new(line, line_num)
      @analyzers.push(obj)
      line_num += 1
    end    
    return @analyzers
  end

  def calculate_line_with_highest_frequency()
# Implement the calculate_line_with_highest_frequency() method to:
  #* calculate the maximum value for highest_wf_count contained by the LineAnalyzer objects in analyzers array
  #  and stores this result in the highest_count_across_lines attribute.
  #* identifies the LineAnalyzer objects in the analyzers array that have highest_wf_count equal to highest_count_across_lines 
  #  attribute value determined previously and stores them in highest_count_words_across_lines.

    #get highest highest_wf_count per analyzer
    max_freque = []    
    @analyzers.each { |analyzer| 
      max_freque.push(analyzer.highest_wf_count)   
    }

    #store the highest highest_wf_count of test.txt
    @highest_count_across_lines = max_freque.max   

    #store analyzers with the  highest highest_wf_count   
    @highest_count_words_across_lines = []
    @analyzers.each { |analyzer| 
      if(analyzer.highest_wf_count == @highest_count_across_lines)
        @highest_count_words_across_lines.push(analyzer)   
      end
    }
  end

  def print_highest_word_frequency_across_lines()
#Implement the print_highest_word_frequency_across_lines() method to
  #* print the values of objects in highest_count_words_across_lines in the specified format
  result = []
  @highest_count_words_across_lines.each{ |analyzer|
    analyzer.highest_wf_words.reverse.each { |word| 
      result.unshift(word)      
    }    
  }
  return result
end
end


















